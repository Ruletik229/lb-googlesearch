document.addEventListener('DOMContentLoaded', () => {
    const input = document.getElementById('text');
    const button = document.getElementById('button');
    const inputbox = document.getElementById(`inputbox`);

    const close = document.getElementById(`closeImg`);
    const imgIcon = document.getElementById(`imgIcon`);
    const searchImgBlock = document.getElementById(`searchImgBlock`);
    const button2 = document.getElementById(`button2`);
    const imageUploadInput = document.getElementById(`imageUploadInput`);


    input.addEventListener('input', () => {
        const inputValue = input.value;

        if (inputValue === '') {
            button.classList.remove('hover');
            button.setAttribute('disabled', 'true');
            button.style.backgroundColor = `#bababa9c`;
            button.style.cursor = `inherit`;
        } else {
            button.removeAttribute('disabled');
            button.classList.add('hover');
            button.style.backgroundColor = `#4285f4`;
            button.style.cursor = `pointer`;
        }
    });

    imageUploadInput.addEventListener(`input`, () => {
        const inputValueImg = imageUploadInput;

        if (inputValueImg === '') {
            button2.classList.remove('hover');
            button2.setAttribute('disabled', 'true');
            button2.style.backgroundColor = `#bababa9c`;
            button2.style.cursor = `inherit`;
        } else {
            button2.removeAttribute('disabled');
            button2.classList.add('hover');
            button2.style.backgroundColor = `#4285f4`;
            button2.style.cursor = `pointer`;
        }
    })

    button.addEventListener('click', () => {
        const url = 'https://www.google.com/search?q=' + encodeURIComponent(input.value);
        window.open(url, '_blank');

        document.getElementById('lucky').href = url;
    });

    button2.addEventListener('click', () => {
        const url = 'https://lens.google.com/search?ep=' + encodeURIComponent(imageUploadInput.value);
        window.open(url, '_blank');
    });

    input.addEventListener('keyup', (event) => {
        if (event.key === 'Enter') {

            const url = 'https://www.google.com/search?q=' + encodeURIComponent(input.value);
            window.open(url, '_blank');
        }
    });

    imgIcon.addEventListener(`click`, () => {
        inputbox.style.display = `none`;
        button.style.display = `none`;
        searchImgBlock.style.display = `block`;
    })

    close.addEventListener(`click`, () => {
        inputbox.style.display = `block`;
        button.style.display = `block`;
        searchImgBlock.style.display = `none`;
    })
});